﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReverseString
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var data = displayMulti("IQRA", 3);

            var dataReverse = displayMulti("OR", 3);

            Console.WriteLine(data);

            Console.WriteLine(dataReverse);

            Thread.Sleep(10000);
        }

        public static string displayMulti(string inputString, int looping)
        {
            var result = string.Empty;

            if (!string.IsNullOrEmpty(inputString) && looping > 0)
            {
                if (inputString.Length < 3)
                {
                    for (int i = 0; i < looping; i++)
                    {
                        char[] charArray = inputString.ToCharArray();
                        Array.Reverse(charArray);
                        result += new string(charArray);
                    }
                }
                else
                {
                    for (int i = 0; i < looping; i++)
                    {
                        result += inputString.Substring(0, 3);
                    }
                }
            }

            return result;
        }
    }
}
