﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;


namespace TestOOP
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string txtInput = "Charlie,Zoe,08123123123;Andre,Xavier,08111222333;Charlie,Xyz,08123123123;Jean,Summers,08001001001";

            var dataInsert = insertData(txtInput);

            Console.WriteLine("=== Output START ===");

            if (dataInsert.Count > 0)
            {
                List<PhoneContact> listPhoneContact = new List<PhoneContact>();

                Console.WriteLine("Log:");

                foreach (var item in dataInsert)
                {
                    string[] itemSplit = item.Split(',');

                    var checkDuplicate = listPhoneContact.Where(x => x.Phone == itemSplit[2]);

                    string status = "duplicate phone number";

                    var _name = string.Format("{0} {1}", itemSplit[0], itemSplit[1]);
                    var _phone = itemSplit[2];

                    if (checkDuplicate.Count() == 0)
                    {
                        listPhoneContact.Add(new PhoneContact()
                        {
                            Name = _name,
                            Phone = _phone
                        }
                        );

                        status = "insert success";
                    }

                    Console.WriteLine(string.Format("{0} - {1} : {2}", _name, _phone, status));
                }

                Console.WriteLine("");
                Console.WriteLine("Phone Book");

                foreach (var dataPhoneBook in listPhoneContact.OrderBy(o => o.Name))
                {
                    Console.WriteLine(string.Format("{0} - {1}", dataPhoneBook.Name, dataPhoneBook.Phone));
                }
            }

            Console.WriteLine("=== Output END ===");

            Thread.Sleep(10000);
        }

        private static List<string> insertData(string inputData)
        {
            var listData = new List<string>();
            if (!string.IsNullOrEmpty(inputData))
            {
                string[] stringSplit = inputData.Split(';');

                for (int item = 0; item < stringSplit.Count(); item++)
                {
                    string[] itemSplit = stringSplit[item].Split(',');
                    if (itemSplit.Count() != 3)
                    {
                        listData = new List<string>();
                        break;
                    }

                    listData.Add(stringSplit[item]);
                }
            }
            return listData;
        }

        public class PhoneContact
        {
            public string Name { get; set; }
            public string Phone { get; set; }
        }

    }
}
